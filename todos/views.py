from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import TodoList, TodoItem

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "list.html"

class TodoDetailView(DetailView):
    model = TodoList
    template_name = "detail.html"

class TodoCreateView(CreateView):
    model = TodoList
    template_name = "create.html"
    fields = ["name"]
    success_url = reverse_lazy("list_todos")

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])

class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "delete.html"

    def get_success_url(self):
        return reverse_lazy("list_todos")
    
class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "create_item.html"
    success_url = reverse_lazy("list_todos")

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "edit_item.html"

    # success_url = reverse_lazy("list_todos")

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])